#include "../hdr/Application/App.h"

using namespace Application;

int main(void)
{
    App::run();

    return 0;
}
