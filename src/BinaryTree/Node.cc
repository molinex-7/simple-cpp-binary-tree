#include "../../hdr/BinaryTree/Node.h"

using namespace BinaryTree;

Node::Node()
{
    this->value = 0;
    this->side = 'T';
    this->right = NULL;
    this->left  = NULL;
}

void Node::setNodeValue(int value)
{
    this->value = value;
}

int Node::getNodeValue()
{
    return this->value;
}

void Node::setNodeSide(char side)
{
    this->side = side;
}

char Node::getNodeSide()
{
    return this->side;
}

void Node::setNodeLeftPointer(Node * left)
{
    this->left = left;
}

Node * Node::getNodeLeftPointer()
{
    return this->left;
}

void Node::setNodeRightPointer(Node * right)
{
    this->right = right;
}

Node * Node::getNodeRightPointer()
{
    return this->right;
}
