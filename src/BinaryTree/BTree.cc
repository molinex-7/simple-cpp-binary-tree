#include "../../hdr/BinaryTree/BTree.h"

using namespace BinaryTree;

BTree::BTree()
{
    this->root = NULL;
}

/* private */
void BTree::destroyTree(Node * leaf)
{
    if (leaf != NULL) {
        this->destroyTree(leaf->getNodeRightPointer());
        this->destroyTree(leaf->getNodeLeftPointer());
        delete leaf;
    }
}

/* public */
void BTree::destroyTree()
{
    this->destroyTree(this->root);
}

/* private */
void BTree::insert(int key, Node * leaf)
{
    if (key < leaf->getNodeValue()) {
        if (leaf->getNodeLeftPointer() != NULL) {
            this->insert(key, leaf->getNodeLeftPointer());
        } else {
            Node * n = new Node();
            n->setNodeValue(key);
            n->setNodeSide('E');

            leaf->setNodeLeftPointer(n);
        }
    } else if (key >= leaf->getNodeValue()) {
        if (leaf->getNodeRightPointer() != NULL) {
            this->insert(key, leaf->getNodeRightPointer());
        } else {
            Node * n = new Node();
            n->setNodeValue(key);
            n->setNodeSide('R');

            leaf->setNodeRightPointer(n);
        }
    }
}

/* protected */
void BTree::insert(int key)
{
    if (this->root != NULL) {
        this->insert(key, this->root);
    } else {
        this->root = new Node();
        this->root->setNodeValue(key);
    }
}

/* private */
Node * BTree::search(int key, Node * leaf)
{
    if (leaf != NULL) {
        if (key == leaf->getNodeValue()) return leaf;
        
        if (key < leaf->getNodeValue()) 
            return this->search(key, leaf->getNodeLeftPointer());
        else
            return this->search(key, leaf->getNodeRightPointer());
    } else return NULL;
}

/* protected */
Node * BTree::search(int key)
{
    return this->search(key, this->root);
}

