#include "../../hdr/BinaryTree/BTreeView.h"

using namespace BinaryTree;

void BTreeView::generateBTree(size_t size)
{
    char opt;
    long unsigned int i;
    unsigned int random;

    cout << "Generate aleatories keys..." << endl;
    cout << "This are the keys that we generate:" << endl << endl;
    cout << "[ ";

    srand(time(NULL));

    for (i = 0; i < size; i++) {
        random = rand() % 100;
        this->leafKeys.push_back(random);

        cout << random << " ";
    }

    cout << "]" << endl << endl;
    cout << "Could we make our tree with this? [y/n] ";
    cin >> opt;

    if (opt == 'y') {
        for (int key : this->leafKeys) 
            this->insert(key);
    } else {
        this->leafKeys.clear();
        this->generateBTree(size);
    }
}

Node * BTreeView::searchForRootNode()
{
    return this->search(this->leafKeys.at(0));
}

/* public */
void BTreeView::printTree()
{
    this->printTree(this->searchForRootNode());
}

/* private */
void BTreeView::printTree(Node * root)
{
    if (root == NULL) {
        cout << "We could not find the root node..." << endl;
        return;
    }

    cout << root->getNodeValue() << endl;
    this->printSubTree(root, "");
    cout << endl;
}

void BTreeView::printSubTree(Node * root, const string& prefix)
{
    bool hasLeft = (root->getNodeLeftPointer() != NULL);
    bool hasRight = (root->getNodeRightPointer() != NULL);

    if (!hasLeft && !hasRight) return;

    cout << prefix; 
    cout << ((hasLeft && hasRight)? "├──": "");
    cout << ((!hasLeft && hasRight)? "└──": "");

    if (hasRight) {
        Node * rightLeaf = root->getNodeRightPointer();
        bool printStrand = (
                hasLeft 
                && hasRight 
                && (
                        rightLeaf->getNodeRightPointer() != NULL 
                        || rightLeaf->getNodeLeftPointer() != NULL
                   )
        );
        string newPrefix = prefix + (printStrand ? "│   " : "    ");

        cout << rightLeaf->getNodeValue() << endl;

        this->printSubTree(rightLeaf, newPrefix);
    }

    if (hasLeft) {
        Node * leftLeaf = root->getNodeLeftPointer();

        cout << (hasRight ? prefix : "") 
            << "└── " 
            << leftLeaf->getNodeValue()
            << endl;

        this->printSubTree(leftLeaf, prefix + "    ");
    }
}

/* public */
void BTreeView::printTreePreOrder()
{
    this->printTreePreOrder(this->searchForRootNode());
}

/* private */
void BTreeView::printTreePreOrder(Node * node)
{
    if (node != NULL) {
        cout << node->getNodeValue();

        if (node->getNodeSide() == 'E') 
            cout << "left "; 
        else if (node->getNodeSide() == 'R') 
            cout << "right "; 
        else
            cout << "root "; 

        this->printTreePreOrder(node->getNodeLeftPointer());
        this->printTreePreOrder(node->getNodeRightPointer());
    }
}

/* public */
void BTreeView::printTreeOrder()
{
    this->printTreeOrder(this->searchForRootNode());
}

/* private */
void BTreeView::printTreeOrder(Node * node)
{
    if (node != NULL) {
        this->printTreePreOrder(node->getNodeLeftPointer());

        cout << node->getNodeValue();

        if (node->getNodeSide() == 'E') 
            cout << "left "; 
        else if (node->getNodeSide() == 'R') 
            cout << "right "; 
        else
            cout << "root "; 

        this->printTreePreOrder(node->getNodeRightPointer());
    }
}

/* public */
void BTreeView::printTreePosOrder()
{
    this->printTreePosOrder(this->searchForRootNode());
}

/* private */
void BTreeView::printTreePosOrder(Node * node)
{
    if (node != NULL) {
        this->printTreePreOrder(node->getNodeLeftPointer());
        this->printTreePreOrder(node->getNodeRightPointer());

        cout << node->getNodeValue();

        if (node->getNodeSide() == 'E') 
            cout << "left "; 
        else if (node->getNodeSide() == 'R') 
            cout << "right "; 
        else
            cout << "root "; 
    }
}

