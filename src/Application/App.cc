#include "../../hdr/Application/App.h"

using namespace Application;
using namespace BinaryTree;
using namespace std;

/* static */
void App::run()
{
    size_t size;
    int opt;
    BTreeView btv = BTreeView();

    cout << "How many leaves will have our tree: ";
    cin >> size;

    btv.generateBTree(size);

    while (1) {
        cout << endl;
        cout << "Ok, we have our tree. Now you want:" << endl;
        cout << "[1] Print our entire tree" << endl;
        cout << "[2] Print our tree in pre order" << endl;
        cout << "[3] Print our tree in order" << endl;
        cout << "[4] Print our tree in pos order" << endl;
        cout << "[0] Exit" << endl << endl;
        cout << "As Shao Khan would say, choose your destiny: ";

        cin >> opt;

        if (opt == 1) {
            cout << endl << endl;
            btv.printTree();
        } else if (opt == 2) {
            cout << endl << endl;
            btv.printTreePreOrder();
            cout << endl << endl;
        } else if (opt == 3) {
            cout << endl << endl;
            btv.printTreeOrder();
            cout << endl << endl;
        } else if (opt == 4) {
            cout << endl << endl;
            btv.printTreePosOrder();
            cout << endl << endl;
        } else {
            cout << endl << "Aborting..." << endl;
            break;
        }
    }

    btv.destroyTree();
}
