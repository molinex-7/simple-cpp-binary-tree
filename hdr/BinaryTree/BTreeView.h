#ifndef __BTREEVIEW__
#define __BTREEVIEW__ 

#include "../includes.h"
#include "Node.h"
#include "BTree.h"

using namespace std;

namespace BinaryTree {
    /*! \class BTreeView
     *  \brief BTreeView class
     *
     *  Class to print our tree
     *  information on screen
     */
    class BTreeView : public BTree
    {
        private:
            vector<int> leafKeys;

            Node * searchForRootNode();
            void printTree(Node *);
            void printSubTree(Node *, const string&);
            void printTreePreOrder(Node *);
            void printTreeOrder(Node *);
            void printTreePosOrder(Node *);

        public:
            BTreeView() : BTree () { }

            void generateBTree(size_t);
            void printTree();
            void printTreePreOrder();
            void printTreeOrder();
            void printTreePosOrder();
    };
} /* namespace BinaryTree */
#endif /* ifndef __BTREEVIEW__ */
