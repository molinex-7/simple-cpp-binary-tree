#ifndef __BTREE__H
#define __BTREE__H 

#include "../includes.h"
#include "Node.h"

namespace BinaryTree {
    /*! \class BTree
     *  \brief Implementation of a Binary Tree
     *
     *  This manipulate the Nodes to make our Binary Tree
     */
    class BTree
    {
        private:
            Node * root;

            void insert(int, Node *);
            Node * search(int, Node *);
            void destroyTree(Node *);

        protected:
            void insert(int);
            Node * search(int);

        public:
            BTree();

            void destroyTree();
    };
} /* namespace BTree */
#endif /* ifndef __BTREE__H */
