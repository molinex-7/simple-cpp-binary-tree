#ifndef __NODE__H
#define __NODE__H 

#include "../includes.h"

namespace BinaryTree {
    /*! \class Node
     *  \brief Implements Nodes of a Tree
     *
     *  Implementation Nodes of a tree, data, getters and setters
     */
    class Node
    {
        private:
            int value;
            char side;
            Node * left;
            Node * right;

        public:
            Node();

            void setNodeValue(int);
            int getNodeValue();
            void setNodeSide(char);
            char getNodeSide();
            void setNodeLeftPointer(Node *);
            Node * getNodeLeftPointer();
            void setNodeRightPointer(Node *);
            Node * getNodeRightPointer();
    };
} /* namespace BinaryTree */
#endif /* ifndef __NODE__H */
