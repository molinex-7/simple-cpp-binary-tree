#ifndef __APP__H
#define __APP__H 

#include "../includes.h"
#include "../BinaryTree/BTreeView.h"

namespace Application {
    /*! \class App
     *  \brief App class
     *
     *  This class runs our program, 
     *  to make our binary tree
     */
    class App
    {
        public:
            static void run();
    };
} /* namespace Application */
#endif /* ifndef __APP__H */
