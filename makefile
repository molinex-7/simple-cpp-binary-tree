###############################################################################
# Vars
###############################################################################
DIR=@mkdir -p $(@D)
CC=g++
CFLAGS=-Wall -Wextra -pedantic -std=c++1z
EXEC=binary_tree
FILES= \
	   BinaryTree/Node \
	   BinaryTree/BTree \
	   BinaryTree/BTreeView \
	   Application/App

###############################################################################
# func
###############################################################################

define generateRules
$(1:%=obj/%.o): $(1:%=src/%.cc) $(1:%=hdr/%.h)
	$$(DIR)
	$$(CC) $$(CFLAGS) -o $$@ -c $$< 
endef

###############################################################################
# build
###############################################################################

$(EXEC): $(FILES:%=obj/%.o) obj/main.o
	$(CC) -o $@ $^ 

$(foreach f, $(FILES), $(eval $(call generateRules, $(f))))

obj/main.o: src/main.cc
	$(DIR)
	$(CC) $(CFLAGS) -o $@ -c $< 

.PHONY: install clean

install: $(EXEC)
	@mv $(EXEC) bin

clean:
	@rm -rf obj

