# Simple cpp binary tree

A Simple binary tree made in cpp 17, i think...

### Description

ok, it was a long time since I fed my repositories on gitlab. 
I'm working on a project with PHP, Laravel, that stuff, 
and frankly, this is hard to find time for anything else.

But every now and then it is good to clear your mind and 
do something different. This is why to plant this little tree. 
This little bonsai.

And this is a good idea. Whenever I am stressed, I will plant a 
different binary tree, to pass the time.

It was also a long time since I even started my notebook (ten 
years old). I could only handle the company's notebook. And 
being able to play with this old notebook, archlinux, i3, vim, 
g ++, was very relaxing too.

![Screenshot](new_screenshot.png)

### Run

Prepare the environment:

    mkdir {bin,obj}

Compile with:

    make install clean

Run with:

    bin/binary_tree


### Contact

For any clarification contact us

	Mail: t.molinex@gmail.com

